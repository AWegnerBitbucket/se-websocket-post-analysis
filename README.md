The SQLite3 database is to large to store in this repository. It is available on Google Drive, [here](https://drive.google.com/file/d/0B6YyF69UI34bZlZMRDBRVkZuT00/edit)

The SQLite3 database is populated from the Stack Exchange [real time](http://stackexchange.com/questions?tab=realtime) question feed. The information gathered from this feed is passed through various regular expressions that Stack Exchange community members utilize to look for low quality/spam posts.

Regular expressions are taken from the [SmokeDetector](https://github.com/Charcoal-SE/SmokeDetector/blob/master/findspam.py) project and [Jan Dvorak](https://github.com/honnza/se-spam-helper/blob/master/se-spam-helper.user.js)'s user script that watches the real time feed as well. As new Regular expressions are discovered, my plan is to add them in as well. Hopefully a viable system can be built to perform analysis on low quality posts.


### Tables

 - `settings` - This table will, eventually be utilized by my behind the scenes process. At this point in time, it does nothing
 - `posts` - Contains all information available via the real time feed's web sockets. Each post is loaded into the table and an auto incrementing number is assigned as well. This allows duplicates (ie. a post was answered/modified) to be inserted without issue. This auto incrementing value should not be used for linking to any other table. It is added purely to make my life easier.
 - `detection_rules` - This contains a listing of all regexs currently validated against. *NOTE*: Rule 1 indicates good content and Rule 2 indicates a post that was flagged as spam in a chat room (ie. a user posted a link). Rule 2 is not automatically added to the system. It is added when I see the notice in the chatroom and when a non-1 entry does not exist in this table.
 - `post_types` - This table contains the `post_id` and `rule_id` associated with it, based on which regular expressions matched.


### Known issues
Several of the tables are lacking indexes on key fields. That will be fixed when I get annoyed by slow query times.

### Dataset Statistics 

This upload occurred at 2014-08-19 14:54:41 Central time. There are currently **318461** posts in the database and **5411** have been classified as something other than "Good". Note that a single post can be classified under more than 1 detection rule.

The oldest question in this data set is from **2014-07-27 15:33:30**
The newest question in this data set is from **2014-08-19 14:54:24**

These are simple break downs of the counts:

Count per detection type:

| cnt | Value |
|:----|:------|
| 313021 | No matching rules (Good Content) |
| 5147 | Jan Dvorak User Script RegEx |
| 143 | All caps title detected |
| 74 | Phone number detected |
| 50 | Added from chat link |
| 27 | Offensive title detected |
| 20 | Bad keyword detected |


Count per site:

| cnt | Value |
|:----|:------|
| 106158 | stackoverflow |
| 33522 | math |
| 14657 | askubuntu |
| 13834 | superuser |
| 7538 | serverfault |
| 6834 | unix |
| 5691 | physics |
| 5612 | tex |
| 5213 | english |
| 4553 | stats |
| 4381 | gis |
| 4260 | codereview |
| 3758 | electronics |
| 3655 | pt.stackoverflow |
| 3496 | wordpress |
| 3376 | programmers |
| 3370 | magento |
| 3361 | sharepoint |
| 3178 | gaming |
| 3136 | apple |
| 3109 | drupal |
| 3105 | codegolf |
| 3092 | dba |
| 2927 | mathematica |
| 2911 | salesforce |
| 2891 | scifi |
| 2754 | mathoverflow.net |
| 2197 | android |
| 2159 | ell |
| 2053 | security |
| 1878 | rpg |
| 1840 | gamedev |
| 1712 | workplace |
| 1668 | meta |
| 1528 | graphicdesign |
| 1474 | travel |
| 1454 | diy |
| 1449 | ux |
| 1270 | webapps |
| 1170 | judaism |
| 1167 | academia |
| 1149 | christianity |
| 1116 | blender |
| 1098 | chemistry |
| 988 | webmasters |
| 966 | softwarerecs |
| 962 | movies |
| 889 | biology |
| 881 | islam |
| 798 | money |
| 796 | cs |
| 765 | cooking |
| 724 | anime |
| 722 | photo |
| 716 | craftcms |
| 703 | music |
| 695 | skeptics |
| 685 | buddhism |
| 665 | raspberrypi |
| 607 | cs50 |
| 607 | expressionengine |
| 599 | crypto |
| 592 | hinduism |
| 585 | bitcoin |
| 581 | philosophy |
| 564 | german |
| 551 | dsp |
| 494 | bicycles |
| 487 | aviation |
| 479 | hermeneutics |
| 479 | networkengineering |
| 468 | arduino |
| 461 | mechanics |
| 443 | french |
| 437 | chinese |
| 419 | japanese |
| 410 | outdoors |
| 409 | history |
| 408 | gardening |
| 386 | quant |
| 375 | moderators |
| 360 | tor |
| 355 | startups |
| 346 | joomla |
| 337 | fitness |
| 330 | boardgames |
| 312 | tridion |
| 260 | cstheory |
| 260 | reverseengineering |
| 260 | sqa |
| 252 | linguistics |
| 252 | scicomp |
| 247 | astronomy |
| 236 | chess |
| 226 | pets |
| 224 | russian |
| 221 | patents |
| 217 | puzzling |
| 216 | space |
| 212 | sports |
| 205 | writers |
| 198 | video |
| 196 | pm |
| 185 | politics |
| 184 | spanish |
| 179 | parenting |
| 172 | sound |
| 170 | datascience |
| 166 | cogsci |
| 163 | matheducators |
| 149 | windowsphone |
| 148 | earthscience |
| 145 | italian |
| 136 | expatriates |
| 131 | genealogy |
| 127 | opendata |
| 127 | robotics |
| 123 | edx-cs169-1x |
| 121 | bricks |
| 117 | freelancing |
| 90 | homebrew |
| 88 | productivity |
| 84 | martialarts |
| 69 | ham |
| 61 | stackapps |
| 54 | ebooks |
| 52 | sustainability |
| 43 | poker |
| 41 | beer |
| 37 | gametheory |
| 27 | cg |
| 1 | operatingsystems |


--- End Dataset Statistics 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 